#+TITLE: A template for scientific papers based on Org-Mode and \LaTeX 
#+AUTHOR: Y. Zerlaut

* Preamble (options for LaTeX formatting) :noexport:

** basic setup
#+LATEX_CLASS: article
#+OPTIONS: toc:nil        (no Table Of COntents at all)
#+LaTeX_CLASS_OPTIONS: [11pt,a4paper,twoside,twocolumn,colorlinks]
#+LaTeX_HEADER:\usepackage{graphicx}
# #+LaTeX_HEADER:\usepackage[AUTO]{inputenc}
#+LaTeX_HEADER:\usepackage[T1]{fontenc}
#+LaTeX_HEADER:\usepackage{lmodern}
#+LaTeX_HEADER:\usepackage{amssymb,mathenv,array}
#+LaTeX_HEADER:\usepackage{microtype} % Slightly tweak font spacing for aesthetics
#+LaTeX_HEADER: \usepackage[labelfont=bf]{caption}


** Geometry
#+LaTeX_HEADER: \usepackage{geometry}
#+LaTeX_HEADER: \geometry{a4paper,total={210mm,297mm}, left=20mm, right=20mm, top=20mm, bottom=20mm, bindingoffset=0mm, columnsep=.8cm}

# so to remember
# 2 columns figure : 17cm
# 1 column figure : 7.7cm


** Section numbering

#+LaTeX_HEADER: \renewcommand\thesection{\Roman{section}}
# #+LaTeX_HEADER: \titleformat{\section}[block]{\Large\bfseries}{\thesection.}{.5em}{} 
# #+LaTeX_HEADER:\renewcommand{\@fnsymbol}[1]{ \ensuremath{    \ifcase#1  \or *    \or \dagger    \or \ddagger    \or \mathsection    \or \mathparagraph    \else      \@ctrerr    \fi }}

# #+options: num:nil ### TO REMOVE NUMBERING


** abstract settings
# #+LaTeX_HEADER: \usepackage{abstract}
# #+LaTeX_HEADER: \renewcommand\abstracttextfont{\bfseries}
# #+LaTeX_HEADER: \renewcommand\abstractnamefont{\normalfont\small\bfseries}
# #+LaTeX_HEADER: \setlength\absleftindent{4mm}
# #+LaTeX_HEADER: \setlength\absrightindent{5mm}


** hyperref
#+LaTeX_HEADER: \hypersetup{allcolors = blue} % to have all the hyperlinks in 1 color
# #+LaTeX_HEADER: \def\appendixautorefname{appendix}


** Headers and footers
#+LaTeX_HEADER: \usepackage{fancyhdr} % Headers and footers
#+LaTeX_HEADER: \pagestyle{fancy} % All pages have headers and footers
#+LaTeX_HEADER: \fancyhead{} % Blank out the default header
#+LaTeX_HEADER: \fancyfoot{} % Blank out the default footer
#+LaTeX_HEADER: \fancyhead[C]{\footnotesize \shorttitle \quad $\bullet$ \quad \shortauthor \quad $\bullet$ \quad \shortdate \normalsize }
#+LaTeX_HEADER: \fancyfoot[C]{\thepage} % Custom footer text
#+LaTeX_HEADER: \makeatletter


** Title and Authors

#+LaTeX_HEADER: \usepackage{titlesec} % Allows customization of titles
## ## WE EXPLICIT THE FOOTNOTEMARK IN THE AUTHORS (for easier change) :
#+LaTeX_HEADER:\renewcommand{\@fnsymbol}[1]{  \ensuremath{    \ifcase#1  \or *    \or \dagger    \or \ddagger    \or \mathsection    \or \mathparagraph    \else      \@ctrerr    \fi  } }

#+LaTeX_HEADER: \def\@maketitle{  \newpage  \null  \vspace{-10mm}   \begin{center}  \let \footnote \thanks    {\LARGE \textbf{\@title} \par}    \vskip 1.2em    {\large      \lineskip .5em      \begin{tabular}[t]{c}        \scshape      \normalsize        \@author      \end{tabular}\par}   \vskip .6em   { \@date}  \end{center}  \par  \vskip 1em}
#+LaTeX_HEADER: \makeatother
#+LaTeX_HEADER: \renewcommand{\thefootnote}{\arabic{footnote}} % we restor the arabic number footnotes


** Short titles/author

#+LaTeX_HEADER: \def\shorttitle{\LaTeX  and Org-Mode} 
#+LaTeX_HEADER: \def\shortauthor{Y. Zerlaut} 
#+LaTeX_HEADER: \def\shortdate{\today} 


** Blind Text for this example
#+LaTeX_HEADER: \usepackage{blindtext} 



** supplementary
#+LaTeX_HEADER: \newcommand{\beginsupplement}{\setcounter{table}{0} \renewcommand{\thetable}{S\arabic{table}} \setcounter{figure}{0} \renewcommand{\thefigure}{S\arabic{figure}}  }




** biblio

# we delete the references name
#+LaTeX_HEADER: \renewcommand{\refname}{\vspace{-.8cm}}

# package to generate a file
#+LaTeX_HEADER: \usepackage{filecontents}


* Introduction

\blindtext[2]

\blindtext[1]


* Methods

\blindtext[1]

** Some equations
:PROPERTIES:
:CUSTOM_ID: m1
:END:

   1. Introduction

\begin{equation}
\label{eq1}
C_m \frac{dV}{dt} = g_L \cdot (E_L - V) + \sum_{\{t_i\}} \delta(t_i)
\end{equation}

   2. Main part

Can we refer to [[eq1][Equation 1]] ?

   3. Questions
    
\begin{equation}
\label{eq:2}
\mathbb{N}(x) = \frac{1}{\sigma \sqrt{2 \pi}} 
\cdot \mathrm{e}^{-\frac{(\mu - x)^2}{2 \sigma^2}}
\end{equation}
    

\blindtext[2]


** Some interesting stuff
    
*** an important point
    
    - subpoint a
      
    - subpoint


* Results

First we will reference to the [[#m1][Section 1]]

** Section associated to the first figure

#+ATTR_LATEX: width=\linewidth
#+label: fig:sfig1
#+caption: Example of a classical one column figure
[[file:fig.pdf]]


#+ATTR_LATEX: width=\linewidth :float multicolumn
#+label: fig:sfig1
#+caption: Example of a two columns figure
[[file:sfig.pdf]]

*** generating the first figure :noexport:

#+begin_src python
import matplotlib.pylab as plt
import matplotlib
font = {'size'   : 10}
matplotlib.rc('font', **font)
import numpy as np
import sys
sys.path.append('/home/yann/work/python_library/')
from my_graph import adjust_spines
plt.figure(figsize=(7.7*0.4, 5*0.4))
plt.subplots_adjust(bottom=.29, left=.21)
ax = plt.subplot(111)
plt.hist(np.random.randn(1000), bins=50)
adjust_spines(ax, ['left', 'bottom'])
ax.set_xlabel('x (units)')
ax.set_ylabel('y (units)')
# plt.show()
plt.savefig('fig.pdf', format='pdf')
#+end_src
#+RESULTS:
: None


*** generating a second figure :noexport:

#+begin_src python
import matplotlib
font = {'size'   : 10}
matplotlib.rc('font', **font)
import matplotlib.pylab as plt
import numpy as np
import sys
sys.path.append('/home/yann/work/python_library/')
from my_graph import adjust_spines
plt.figure(figsize=(17*0.4,4*0.4))
plt.subplots_adjust(left=.1, bottom=.26, right=.98, top=.95)
ax = plt.subplot(131)
plt.hist(np.exp(np.random.randn(100)), bins=50)
adjust_spines(ax, ['left', 'bottom'])
plt.xlabel('x (units)')
plt.ylabel('y (units)')
ax = plt.subplot(132)
plt.hist(np.exp(np.random.rand(100)), bins=50)
adjust_spines(ax, ['left', 'bottom'])
plt.xlabel('x (units)')
plt.ylabel('y (units)')
ax = plt.subplot(133)
plt.hist(np.random.randn(100), bins=50)
adjust_spines(ax, ['left', 'bottom'])
plt.xlabel('x (units)')
plt.ylabel('y (units)')
# plt.show()
plt.savefig('sfig.pdf', format='pdf')
#+end_src
#+RESULTS:
: None


** More interesting stuff

Same text than in abstract, but we cite \cite{Kuhn2004} and
\cite{Brunel2001a}

\blindtext[0]


* Discussion

** sum up

\blindtext[4]



** perspectives

\blindtext[5]


* References

#+BEGIN_LATEX
\begin{filecontents}{biblio.bib}

@article{Kuhn2004,
author = {Kuhn, Alexandre and Aertsen, Ad and Rotter, Stefan},
doi = {10.1523/JNEUROSCI.3349-03.2004},
issn = {1529-2401},
journal = {The Journal of neuroscience : the official journal of the Society for Neuroscience},
keywords = {Action Potentials,Action Potentials: physiology,Animals,Computer Simulation,Excitatory Postsynaptic Potentials,Excitatory Postsynaptic Potentials: physiology,Humans,Membrane Potentials,Membrane Potentials: physiology,Models,Neural Inhibition,Neural Inhibition: physiology,Neurological,Neurons,Neurons: physiology,Synapses,Synapses: physiology,Synaptic Transmission,Synaptic Transmission: physiology,Visual Cortex,Visual Cortex: physiology},
month = mar,
number = {10},
pages = {2345--56},
pmid = {15014109},
publisher = {Soc Neuroscience},
title = {{Neuronal integration of synaptic input in the fluctuation-driven regime.}},
url = {http://www.ncbi.nlm.nih.gov/pubmed/15014109},
volume = {24},
year = {2004}
}

@article{Brunel2001a,
abstract = {Noise can have a significant impact on the response dynamics of a nonlinear system. For neurons, the primary source of noise comes from background synaptic input activity. If this is approximated as white noise, the amplitude of the modulation of the firing rate in response to an input current oscillating at frequency omega decreases as 1/square root[omega] and lags the input by 45 degrees in phase. However, if filtering due to realistic synaptic dynamics is included, the firing rate is modulated by a finite amount even in the limit omega-->infinity and the phase lag is eliminated. Thus, through its effect on noise inputs, realistic synaptic dynamics can ensure unlagged neuronal responses to high-frequency inputs.},
author = {Brunel, Nicolas and Chance, F S and Fourcaud, N and Abbott, L F},
issn = {0031-9007},
journal = {Physical review letters},
keywords = {Action Potentials,Action Potentials: physiology,Mathematical Computing,Models, Neurological,Neurons,Neurons: physiology,Synapses,Synapses: physiology,Synaptic Transmission,Synaptic Transmission: physiology},
mendeley-groups = {Neuroscience/Neuronal models},
month = mar,
number = {10},
pages = {2186--9},
pmid = {11289886},
title = {{Effects of synaptic noise and filtering on the frequency response of spiking neurons.}},
url = {http://www.ncbi.nlm.nih.gov/pubmed/11289886},
volume = {86},
year = {2001}
}

\end{filecontents}
#+END_LATEX

\bibliographystyle{apalike}
\bibliography{biblio}

\onecolumn
\newpage
\twocolumn
\beginsupplement


* Appendix

** fig_s1

#+ATTR_LATEX_latex: :placement [tb]
#+label: fig:fig2
#+caption: \blindtext[0]
[[file:fig.pdf]]

Some discussion about a figure, the Figure \ref{fig:fig2}

Some discussion about the supplementary figure, the Figure
\ref{fig:sfig1}


** some more discussion

   \blindtext[10]
