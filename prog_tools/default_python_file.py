""" documentation for this file is written at the end
 (use of argparse) """

import numpy as np
import argparse

def power_func(x, y):
    """ doc """
    return np.power(x,y)


# in case not used as a modulus
if __name__=='__main__':
    # First a nice documentation 
    parser=argparse.ArgumentParser(description=
     """ 
     description of the whole modulus here
     """
    ,formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("-d", "--default", help="Run a default configuration : x=2, y=3",
                        action="store_true")
    parser.add_argument("-x",help="description of first argument", type=int)
    parser.add_argument("-y",help="description of second argument", type=int)
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    if args.default:
        x, y = 2,3
        print 'Running the default configuration :'
    else:
        x, y = args.x, args.y
        
    if not x and not args.default:
        parser.print_help()
    else:
        print 'x^y = ', power_func(x,y)
    
    
