export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

# ---------- INKSCAPE
alias inkscape='/Applications/Inkscape.app/Contents/MacOS/Inkscape'

# ---------- EMACS
alias emacs='/Applications/Emacs.app/Contents/MacOS/Emacs'
alias em='/Applications/Emacs.app/Contents/MacOS/Emacs -nw'

# ---------- EMACS & ORG-MODE & LATEX
# function for latex compil
tex_compil_func() {
    if [ -d "tex/" ]; then echo ""; else mkdir tex/; fi;
    original_string=$1
    string_to_put='.tex'
    tex_file="${original_string/.org/$string_to_put}"    
    tex_file="${original_string/.org/$string_to_put}"
    if [ -f $tex_file ]; then mv $tex_file tex/; fi;
    pdflatex -shell-escape -interaction=nonstopmode -output-directory=tex/ tex/$tex_file > tex/compil_output
}
bib_compil_func() {
    cd tex/
    if [ ! -f biology_citations.bst ]; then cp ~/work/miscellaneous/TEX/biology_citations.bst ./; fi;
    if [ ! -f biology_citations.sty ]; then cp ~/work/miscellaneous/TEX/biology_citations.sty ./; fi;
    original_string=$1
    string_to_put='.aux'
    aux_file="${original_string/.org/$string_to_put}"
    bibtex -terse $aux_file
    cd ..
}

# PRESENTATION
pres_func() {
    emacs --batch -l $HOME/work/miscellaneous/ORG/org-config-pres.el --file $1 -f org-beamer-export-to-latex
    tex_compil_func $1
}
alias pres=pres_func

# PAPER
paper_func() {
    emacs --batch -l $HOME/work/miscellaneous/ORG/org-config-paper.el --file $1 -f org-latex-export-to-latex
    tex_compil_func $1; bib_compil_func $1; tex_compil_func $1
}
alias paper=paper_func

# REPORT
report_func() {
    emacs --batch -l $HOME/work/miscellaneous/ORG/org-config-report.el --file $1 -f org-latex-export-to-latex
    tex_compil_func $1; bib_compil_func $1; tex_compil_func $1
}
alias report=report_func

# SUPPLEMENTARY
report_func() {
    emacs --batch -l $HOME/work/miscellaneous/ORG/org-config-supp.el --file $1 -f org-latex-export-to-latex
    tex_compil_func $1; bib_compil_func $1; tex_compil_func $1
}
alias supp=supp_func
alias latex_clean_up='rm *.out;rm *.snm;rm *.toc;rm *.log;rm *.aux;rm *.out'

# ---------- GIT
alias gca='git commit -a -m'
alias gps='git push origin master'
alias gpl='git pull origin master'

# ---------- PYTHON
# added by Anaconda3 4.1.0 installer
export PATH="/Users/yzerlaut/anaconda/bin:$PATH"

