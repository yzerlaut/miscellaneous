\documentclass[10pt,a4paper,twocolumn,twoside]{article}

\usepackage{graphicx}
\usepackage[utf8]{inputenc}
\usepackage[fleqn]{amsmath}
\usepackage{amssymb,mathenv,array}
\usepackage[francais,english]{babel}
\usepackage[colorlinks=true]{hyperref}
\hypersetup{allcolors = blue} % to have all the hyperlinks in 1 color
\usepackage{microtype} % Slightly tweak font spacing for aesthetics

\usepackage[textwidth=18cm,columnsep=.8cm,bottom=1.3cm,top=1.3cm]{geometry}
\usepackage[labelfont=bf]{caption}

\usepackage{titlesec} % Allows customization of titles
\renewcommand\thesection{\Roman{section}}
\titleformat{\section}[block]{\Large\bfseries}{\thesection.}{.5em}{} 

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\title{\vspace{-13mm}
\Large\textbf{
Campbell's theorem
}} % Article title

\vspace{-2mm}
\author{
\normalsize
\textsc{Yann Zerlaut}\thanks{\vspace{1mm}
\normalsize \href{mailto:zerlaut@unic.cnrs-gif.fr}{zerlaut@unic.cnrs-gif.fr} \newline % Your email address
Unit\'e de Neurosciences, Information et Complexit\'e \newline
CNRS, 91198 Gif sur Yvette, France}
\vspace{-2mm}
}
\date{\today}

%----------------------------------------------------------------------------------------

\begin{document}

\maketitle
\selectlanguage{english}

We want to demonstrate the following properties, in the case of a
stationary homogenous Poisson point process, known as
\textit{Campbell's theorem}:
\begin{equation}
\label{eq:campbell}
\left\{
  \begin{split}
& \mu_s = \langle s(t) \rangle = \nu
  \int_0^\infty dL \, \, \alpha(L) \\
& \sigma_s  = 
\sqrt{\langle \Big(s(t)- \langle s \rangle \Big)^2 \rangle} = 
 \sqrt{\nu \int_0^\infty dL \, \, \alpha(L)^2 }
  \end{split}
\right. 
\end{equation}


\section{Proof with spike trains}

Let's take a shotnoise
resulting from a point process generating the set of events
$\{t_k\}_{k \in \mathbb{N}}$ convoluted with a waveform
$\alpha(t)$. At one point in time, we have the signal
$$s(t) = \sum_{\{ t_k \}_{k \in \mathbb{N}}} \alpha(t-t_k)$$


 We rewrite $s(t)$ as:
$$s(t) =S(t) * \alpha(t) = \int_0^\infty d\tau \, \, \alpha(\tau) \, 
S(t-\tau)$$
with:
$$ S(\tau) =  \sum_{\{ t_k \}_{k \in \mathbb{N}}} \delta(\tau-t_k) $$
where $\delta(t)$ is the Dirac distribution.\\


We define $S(t)$ as being equal to $\frac{1}{dt}$ in the intervals
$[t_k-\frac{dt}{2},t_k+\frac{dt}{2}], \, \, \forall k$ and 0
elsewhere ($dt$ is an infinitesimal quantity). \\

To proove \textit{Campbell's theorem}, we just need to study the
statistical properties of the function $S(\tau)$. \\

At one point in time, the probability to have a spike in $dt$ is $\nu
\, dt$, so at one point in time the $\langle S(\tau) \rangle = \nu \,
dt \, \, \frac{1}{dt} = \nu$.\\

Now we study the average cross-product $\langle S(\tau_1) \, S(\tau_2)
\rangle$, the Poisson point process generate independent events, so
the probability to have two coincident spike separated by
$|\tau_2-\tau_1|$ in the spike train is exactly the one given by
chance coincidence, so for $\tau_1 \neq \tau_2, \, \, \langle
S(\tau_1) \,
S(\tau_2) \rangle = \nu^2$. \\
For $\tau_2 \in [\tau_1 - \frac{dt}{2}, \tau_1 + \frac{dt}{2}]$, we
have a probability $\nu \, dt$ to have a spike, so $\langle S(\tau_1)
\, S(\tau_2) \rangle = \nu \, dt \, \frac{1}{dt} \, \frac{1}{dt} = \nu
\, \frac{1}{dt}$, after taking the limit $dt \rightarrow 0$, we get:
$$
\langle S(\tau_1) \, S(\tau_2) \rangle = \nu^2 + \nu \, \delta(\tau_1
- \tau_2)
$$

 From those results, it is trivial to deduce \textit{Campbell's theorem}:
$$
\langle s \rangle = \langle S(t)*\alpha(t) \rangle =
\langle S(t) \rangle *\alpha(t) = \nu \cdot 1* \alpha(t)
$$

so, we get the first moment:
$$
\langle s \rangle = \nu \, \, \int_0^\infty \alpha(t) \, dt
$$

Then we calculate $\langle s^2 \rangle$ to get the second moment:
$$
\langle s^2 \rangle = \langle \Big( S(t)*\alpha(t) \Big)^2 \rangle
$$

$$
\langle s^2 \rangle = \langle \Big(
\int_0^\infty d\tau_1 \int_0^\infty d\tau_2 \, \,
\alpha(\tau_1) \alpha(\tau_2)
S(\tau_1) S(\tau_2)
\Big) \rangle
$$

$$
\langle s^2 \rangle =
\int_0^\infty d\tau_1 \int_0^\infty d\tau_2 \, \,
\alpha(\tau_1) \alpha(\tau_2) \, \,
 \langle 
S(\tau_1) S(\tau_2)
\rangle
$$

We replace the average cross product by its expression, we get:
$$
\langle s^2 \rangle =
\nu^2 \Big( \int_0^\infty d\tau_1 
\alpha(\tau) \Big)^2 + 
\nu \, \, \int_0^\infty d\tau \, \, \alpha(\tau)^2
$$

$$
\langle s^2 \rangle =
\langle s \rangle^2 + \,
\nu \, \int_0^\infty d\tau \, \, \alpha(\tau)^2
$$

so, we get the right standard deviation formula.


\section{Proof with explicit time 
  probabilities (to be finished)}

We give a (valid?) demonstration\footnote{though not rigorous, e.g. we
  sum over an inifinite number of preceeding events, the convergence
  of the resulting sum should be established ! We justify it
  qualitatively by stating that the $\alpha$ kernel has a strong
  attenuation with time} of Campbell's theorem. Let's take a shotnoise
resulting from a point process generating the set of events
$\{t_k\}_{k \in \mathbb{N}}$ convoluted with a waveform
$\alpha(t)$. At one point in time, we have the signal
$$s(t) = \sum_{\{ t_k \}_{k \in \mathbb{N}}} \alpha(t-t_k)$$

Where $\{ t_k\}_{k \in \mathbb{N}}$ is the set of presynaptic arriving
random events before $t$ ($ t_k \leq t, \forall k \in \mathbb{N}$),
for the demonstration we index this set by decreasing order of
appearance in time, $t_0$ being the last spike before $t$, $t_1$ the
one before, etc...The stationarity justify that we can look back in
the past of $t$\footnote{if the process starts at $t=0$, for low $t$,
  there is a bias as the interpsike interval can not be higher than
  $t$, we consider only large $t$.}
.\\



Our definition of the homogenous Poisson point process is formulated
in terms of distance between events, so we reformulate the problem in
terms of interspike interval, we set $\tau_k = t_{k+1} - t_k$ for $k
\geq 1$, we also set $\tau_0 = t - t_0$ then the set $\{t_k\}_{k \in
  \mathbb{N^*}}$ is equivalent to the set $\{\tau_k<t\}_{k \in
  \mathbb{N}}$. There is a probability density function $P(\tau_0,
\tau_1, ... )$ that generates this point process.\\


For the homogenous Poisson process, the interpsikes are independent
and the probability distribution can be factorize, i.e. $P(\tau_0,
\tau_1, ... ) = p_\tau(\tau_0) \, p_\tau(\tau_1) \,... $, where
$p_\tau(\tau)$ is the probability density for the time intervals
between pre-synaptic spikes. For the homogenous Poisson process
generated by a frequency $\nu$, we have $p_\tau(\tau)= \nu \, e^{\nu
  \tau}$.\\

From this we want to derive the probability that the $k^{th}$ spike is
at a temporal distance $L_k$ from $t$, i.e. we look for the
probability density of the variable $t-t_k$ as a function of
$k$. $L_k$ is given by:
$$
L_k=t-t_k=\sum_{0 \leq i \leq k} \tau_i
$$
So for the probability density of $L_k$ being at a distance $L$:
$$
p_{L_k}(L) dL = \textnormal{Pr}(\sum_{0 \leq i \leq k} \tau_i \in
[L,L+dL])
$$

We demonstrate by recurrence\footnote{infered, calculating the first
  terms and generalizing. This could be another definition of the
  Poisson point process, the porbability to have $k$ spikes in a time
  interval $L$} that the expression for $p_{L_k}(L)$ is:
$$
p_{L_k}(L) = \nu \, \frac{(\nu \, L)^k e^{-\nu L} }{k!}
$$

We have $p_{L_0}(L)= \nu \, e^{-\nu L}$ and the expression of
$p_{L_{k+1}}(L)$ can be derived from the one of $p_{L_k}(L)$:
$$
p_{L_{k+1}}(L) = \int_{0}^{L} dl \, \, p_{L_{k}}(l) \, \, p_\tau(L-l)
$$

$$
p_{L_{k+1}}(L) = \int_{0}^{L} dl \, \,
\nu \, \frac{(\nu \, l)^k e^{-\nu l} }{k!} \, \, 
\nu \, e^{-\nu (L-l)} 
$$

$$
p_{L_{k+1}}(L) = \frac{\nu^{k+2} e^{- \nu L}}{k!}  
\int_{0}^{L} dl \cdot l^k = \frac{\nu^{k+2} e^{- \nu L}}{k!} 
\, \frac{L^{k+1}}{k+1}
$$
finally:
$$
p_{L_{k+1}}(L) = \nu \, \frac{(\nu \, L)^{k+1} e^{- \nu L}}{(k+1)!} 
$$
Thus we demonstrated by recurrence the expression for the probability
density of $p_{L_k}(L)$.\\

We return to the study of $s(t)$, we view this variable as a sum of
the random variables $x_k(L_k) = \alpha(t-t_k)$. It is not possible to
derive the probability density of $x_k$ in the general case, as to do
so, we need to inverse the function $\alpha(L)$\footnote{in the case
  of exponential synapses, it's possible}, but we can compute the
moments as $s(t)$ is a linear sum of independent terms, indeed:

$$
\langle s(t) \rangle = \langle s \rangle = 
\sum_{\{ t_k \}_{k \in \mathbb{N}}} \langle \alpha(t-t_k) \rangle = 
 \sum_{k \in \mathbb{N}} \langle \alpha(L_k) \rangle
$$
the permutations between sum and trial average comes from the
independence of the $L_k$s\footnote{to be discussed, not so easy to
  justify, $L_k$ should depend on $L_{k-1}$ in a single trial}.
Then we explicit the $ \langle \alpha(L_k) \rangle$ value, we get:

$$
\langle s \rangle = \sum_{k \in \mathbb{N}} \,
\int_0^\infty dL \, \, p_{L_k}(L) \, \alpha(L) 
$$

$$\langle s \rangle = \sum_{k \in \mathbb{N}} \,
\int_0^\infty dL \, \, 
 \nu \, \frac{(\nu \, L)^k e^{-\nu L} }{k!}
\, \alpha(L) 
$$
We can permute the integral and the sum, we get:
$$\langle s \rangle = 
\int_0^\infty dL \, \,  \nu \, e^{-\nu L} \,
\, \alpha(L) 
\sum_{k \in \mathbb{N}} \,
\frac{(\nu \, L)^k }{k!}
$$
We recognize the Taylor expansion of $e^{\nu L}$, that cancels with
the $e^{-\nu L}$ terms, so we get the first stationary moment of the
signal $s(t)$:
$$\langle s \rangle = \nu
\int_0^\infty dL \, \, \alpha(L) 
$$

Now we want the second statistical moment of $s(t)$, to this purpose,
we need to calculate:

$$
\langle s(t)^2 \rangle = 
\langle s^2 \rangle = 
\langle \Big( 
\sum_{k \in \mathbb{N}}  \alpha(L_k) \Big)^2 \rangle
= \langle \Big(
\sum_{i,j \in \mathbb{N}^2}  \alpha(L_i) \alpha(L_j) \Big) \rangle
$$

the trial average permutes with the sum, so:
$$
\langle s^2 \rangle = 
\sum_{i,j \in \mathbb{N}^2}  \langle \alpha(L_i) \alpha(L_j) \rangle
$$

Now to evaluate $\langle \alpha(L_i) \alpha(L_j) \rangle$, we will
split the summing over the $\mathbb{N} \times \mathbb{N}$ domain into
two subdomains: the first subdomain corresponds to the configurations
where the event $t_j$ happenened closer from $t$ than the event $t_i$,
the complementary domain is the inverse situation, both domains gives
the same contribution therefore we can write:

$$
\langle s^2 \rangle = 2 \, 
\sum_{i \in \mathbb{N}} \sum_{j \leq i}
\langle \alpha(L_i) \alpha(L_j) \rangle
$$

then within this domain the mean value of $\langle \alpha(L_i)
\alpha(L_j) \rangle$ will be given by the probability to have the
$j^{th}$ spike at a temporal distance $L = L_j$ from $t$ multiplied by
the probability to have $j-i$ spike at a distance $\Delta L = L_i -
L_j$, this is formalized as:


\begin{equation*}
\begin{split}
\langle s^2 \rangle = 2 \, &
\sum_{i \in \mathbb{N}} \sum_{j \leq i}
\int_0^{\infty} dL \, \, p_{L_j}(L) \alpha(L) \\
& \qquad  \times \, \int_0^\infty d(\Delta L)  p_{L_(i-j)}(\Delta L)
\alpha(L+\Delta L) 
\end{split}
\end{equation*}

We explicit the distributions and then permute the sum and integrals:

\begin{equation*}
\begin{split}
\langle s^2 \rangle = 2 \, &
\sum_{i \in \mathbb{N}} \sum_{j \leq i}
\int_0^{\infty} dL \, \, \nu \, 
\frac{(\nu \, L)^j e^{-\nu L} }{j!} \alpha(L) \\
& \qquad  \times \, \int_0^\infty d(\Delta L)  
\frac{(\nu \, \Delta L)^{(i-j)} e^{-\nu \Delta L} }{(i-j)!}
\alpha(L+\Delta L) \\
\end{split}
\end{equation*}

The sum over $j$ correspond the the binom of Newton:
$$
\nu^i \, (L+\Delta L)^i  = \sum_{j \leq i} \frac{i!}{j! \, (i-j)!}
(\nu \, L)^j \, (\nu \, \Delta L)^{i-j}
$$

\begin{equation*}
\begin{split}
\langle s^2 \rangle = 2 \, \nu^2 \, &
\int_0^{\infty} dL \, \,  \alpha(L)
\int_0^\infty d(\Delta L)  \, \, \alpha(L+\Delta L) \\
& \qquad  \times \, e^{- \nu \, (L + \Delta L)}
\sum_{i \in \mathbb{N}} 
\frac{\nu^i \, (L+\Delta L)^i }{i!}\\
\end{split}
\end{equation*}
We recognize the Taylor expansion of the exponential, so the bottom
line terms cancel.

We introduce the function $f(L) = \int_L ^\infty dl \, \, \alpha(l)$,
after a change of variable we have:
$$
\langle s^2 \rangle = 2 \, \nu^2 \, 
\int_0^{\infty} dL \, \,  \alpha(L) \, \, f(L)
$$
we notice that: $\frac{d \, f}{dL} = f'(L) = -\alpha(L)$ and we
integrate by parts ($\int u' \, v = [u \, v] - \int u \, v'$, with
$u=-f$, $v=f$)

$$
\langle s^2 \rangle = 2 \, \nu^2 \, 
\frac{1}{2} \Big[ - f(L)^2 \Big]_{L=0}^{L=\infty} = 
\langle s \rangle^2 
$$

\quad \\
so from this calculus: $\sigma_s=0$, something is obviously wrong !!


\footnotesize
\bibliographystyle{plain}
\bibliography{/home/yann/files/papers/neuro/neuro_database,/home/yann/files/papers/physics/physics_database,/home/yann/files/papers/maths/maths_database}

\end{document}

